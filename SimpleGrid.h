//This header defines a simple list-based implementation for putting atoms in a cell list and computing neighbors

//vector addition
template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size()==b.size());
    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(),a.end(),b.begin(),std::back_inserter(result), std::plus<T>());
    return result;
};
// sign function
template <typename T> int sgn(T &val) {
    return (T(0) < val) - (val < T(0));
}
//minimum distance in a box
dvec mindist(const dvec &posA,const dvec &posB, const dvec &boxl, double &normSquared)
{
    dvec diff(posA.size());
    normSquared = 0.0;
    for (int dd = 0; dd < posA.size(); ++dd)
    {
        diff[dd]=posA[dd]-posB[dd];
        if (fabs(diff[dd]) > 0.5*boxl[dd])
            diff[dd] -= sgn(diff[dd])*boxl[dd];
        normSquared += diff[dd]*diff[dd];
    };
    return diff;
}


#include <list>

class SimpleNeighbor
{
public:
    dvec Displacement;
    int i,j;
    double normSquared;
    SimpleNeighbor(){};
    SimpleNeighbor(int _i, int _j, dvec _disp,double _n2){i=_i;j=_j;Displacement=_disp;normSquared=_n2;}
};

class SimpleGrid
{
public:
        std::list<int> *GridParticles;
        std::list<int> *CellNeighbors;
        std::list<SimpleNeighbor> *NeighborList;
        ivec Size;
        dvec Box;

        dvec Spacing;

public:
        inline int IndexFromCoordinate(const ivec &coord){ return coord[0]+coord[1]*Size[0]+coord[2]*Size[0]*Size[1];};
        inline int IndexFromCoordinate(const dvec &coord){ return floor(coord[0]/Spacing[0])+floor(coord[1]/Spacing[1])*Size[0] + floor(coord[2]/Spacing[2])*Size[0]*Size[1];};

        SimpleGrid(const vector<dvec> &atoms, const dvec &box, double spacing=2.5);
        ~SimpleGrid();

        //repopulates grid, assuming box is unchanged
        void Repopulate(const vector<dvec> &atoms);
        //constructs a neighbor list for a set of particles
        //void PopulateNeighborList( const vector<dvec> &atoms, double Cutoff, list<int> Particles, list<list<SimpleNeighbor> >&Neighbors);
        void PopulateNeighborList( const vector<dvec> &atoms, double CutoffSquared, list<int> Particles, vector<vector<SimpleNeighbor> >&Neighbors);

        //construct a neighbor list
        void ConstructNeighborList(const vector<dvec> &atoms, double Cutoff);

        //get neighboring cells and get particles in a cell
        void GetCellParticles2(int Index, list<int> &ans);
        const list<int> &GetCellParticles(int Index);

        const list<int> &GetNeighborCells(const dvec &Position);

        //Get the neighbors of a particle if the neighbor list has been constructed
        const list<SimpleNeighbor > &GetNeighbors(int particle);
};

SimpleGrid::SimpleGrid(const vector<dvec> &atoms, const dvec &box, double spacing)
{
    Box = box;
    Spacing.resize(DIM);
    Size.resize(DIM);
    int Prod = 1;
    for (int dd = 0; dd <DIM; ++dd)
    {
        Spacing[dd] = spacing;
        Size[dd] = ceil(Box[dd]/Spacing[dd]);
        Prod *= Size[dd];
    };
    GridParticles = new list<int>[Prod];
    CellNeighbors = new list<int>[Prod];
    //populate the neighbor list
    ivec Coord(DIM,0);
    while (Coord[DIM-1] < Size[DIM-1])
    {
        int Index = IndexFromCoordinate(Coord);
        ivec Coord_It(DIM);
        for (int dd = 0; dd < DIM; ++dd)
            Coord_It[dd] = Coord[dd]<2 ? -2 : -1;

        while(Coord_It[DIM-1] < 2 || (Coord[DIM-1] >= Size[DIM-1]-2 && Coord_It[DIM-1 < 3]))
        {
            ivec Coord_2 = Coord+Coord_It;
            for (int dd = 0; dd < DIM; ++dd)
            {
                if(Coord_2[dd] < 0) Coord_2[dd]+=Size[dd];
                if(Coord_2[dd] >= Size[dd]) Coord_2[dd]-=Size[dd];
            };
            int Index2 = IndexFromCoordinate(Coord_2);
            assert(Index2 >=0);
            CellNeighbors[Index].push_back(Index2);
            Coord_It[0]++;
            int t_d = 0;
            while(t_d< DIM-1 &&(!(Coord_It[t_d] < 2 || (Coord[t_d] >=Size[t_d]-2 && Coord_It[t_d] < 3))))
            {
                Coord_It[t_d] = Coord[t_d] < 2 ? -2 : -1;
                Coord_It[t_d+1]++;
                t_d++;
            };
        };

        Coord[0]++;
        int t_d = 0;
        while(t_d < DIM-1 && Coord[t_d] >=Size[t_d])
        {
            Coord[t_d]=0;
            Coord[t_d+1]++;
            t_d++;
        };

    };
    //now populate grid
    for (int ii = 0; ii < atoms.size(); ++ii)
        GridParticles[IndexFromCoordinate(atoms[ii])].push_back(ii);
    NeighborList = NULL;
};

SimpleGrid::~SimpleGrid()
{
    delete []GridParticles;
    delete []CellNeighbors;
    if(NeighborList)
        delete[] NeighborList;
};


void SimpleGrid::ConstructNeighborList(const vector<dvec> &atoms, double Cutoff)
{
    if(NeighborList)
    {
        if(NeighborList->size() == atoms.size())
        {
            for (int ii = 0; ii < NeighborList->size(); ++ii)
                NeighborList[ii].clear();
        }else{
            delete []NeighborList;
            NeighborList = new list<SimpleNeighbor>[atoms.size()];
        };
    }else{
        NeighborList = new list<SimpleNeighbor>[atoms.size()];
    };

    dvec disp(DIM);
    double n2=0.0;
    for (int ii = 0; ii < atoms.size(); ++ii)
    {
        list<int> cells = GetNeighborCells(atoms[ii]);
        for (list<int>::iterator cells_it = cells.begin(); cells_it != cells.end(); cells_it++)
        {
            list<int> particles = GetCellParticles(*cells_it);
            //list<int> particles;
            //GetCellParticles2(*cells_it,particles);
            for (list<int>::iterator particle_it = particles.begin(); particle_it!= particles.end(); particle_it++)
            {
                n2=0.0;
                disp = mindist(atoms[ii], atoms[*particle_it],Box,n2);
                if( ii != *particle_it && n2 < Cutoff*Cutoff)
                    NeighborList[ii].push_back(SimpleNeighbor(ii,*particle_it,disp,n2));

            };
        };
    };
};

/*
void SimpleGrid::PopulateNeighborList(const vector<dvec> &atoms, double Cutoff, list<int> Particles, list<list<SimpleNeighbor> > &Neighbors)
{
    dvec disp;
    for (list<int>::iterator it = Particles.begin(); it != Particles.end(); it ++)
    {
        list<SimpleNeighbor> TNeighbor;
        list<int> cells = GetNeighborCells(atoms[*it]);

        for (list<int>::iterator cells_it = cells.begin(); cells_it != cells.end(); cells_it++)
        {
            list<int> particles = GetCellParticles(*cells_it);
            //list<int> particles;
            //GetCellParticles2(*cells_it,particles);
            for (list<int>::iterator particle_it = particles.begin(); particle_it !=particles.end(); particle_it++)
            {
                double n2;
                disp = mindist(atoms[*it],atoms[*particle_it],Box,n2);
                if (*it != *particle_it && n2 < Cutoff*Cutoff)
                    TNeighbor.push_back(SimpleNeighbor(*it,*particle_it,disp,n2));
            };
        };
        Neighbors.push_back(TNeighbor);
    };
};
*/

void SimpleGrid::PopulateNeighborList(const vector<dvec> &atoms, double CutoffSquared, list<int> Particles, vector<vector<SimpleNeighbor> > &Neighbors)
{
    dvec disp;
    for (list<int>::iterator it = Particles.begin(); it != Particles.end(); it ++)
    {
        vector<SimpleNeighbor> TNeighbor;
        Neighbors.push_back(TNeighbor);
    };


    for (list<int>::iterator it = Particles.begin(); it != Particles.end(); it ++)
    {
        list<int> cells = GetNeighborCells(atoms[*it]);
        for (list<int>::iterator cells_it = cells.begin(); cells_it != cells.end(); cells_it++)
        {
            list<int> particles = GetCellParticles(*cells_it);
            //list<int> particles;
            //GetCellParticles2(*cells_it,particles);
            for (list<int>::iterator particle_it = particles.begin(); particle_it !=particles.end(); particle_it++)
            {
                if(*it < *particle_it)
                {
                    double n2;
                    disp = mindist(atoms[*it],atoms[*particle_it],Box,n2);
                    if(n2 < CutoffSquared)
                    {
                        Neighbors[*it].push_back(SimpleNeighbor(*it,*particle_it,disp,n2));
                        Neighbors[*particle_it].push_back(SimpleNeighbor(*particle_it,*it,disp,n2));
                    };
                };
            };
        };
    };
};

void SimpleGrid::Repopulate(const vector<dvec> &atoms)
{
    int Prod = 1;
    for (int dd = 0; dd < DIM; ++dd)
        Prod *= Size[dd];
    for (int ii = 0; ii < Prod; ++ii)
        GridParticles[ii].clear();
    for (int ii = 0; ii < atoms.size(); ++ii)
        GridParticles[IndexFromCoordinate(atoms[ii])].push_back(ii);
};

void SimpleGrid::GetCellParticles2(int Index, list<int> &ans)
{
    ans = GridParticles[Index];
};


const list<int> &SimpleGrid::GetNeighborCells(const dvec &Position)
{
    return CellNeighbors[IndexFromCoordinate(Position)];
}

const list<int> &SimpleGrid::GetCellParticles(int Index)
{
    return GridParticles[Index];
};

const list<SimpleNeighbor > &SimpleGrid::GetNeighbors(int particle)
{
    if(NeighborList)
        return NeighborList[particle];
    return list<SimpleNeighbor>();
};
