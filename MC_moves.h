//In this section we define a handful of chain transformations to be used in Monte Carlo moves
//The chains passed to these functions should all be in unwrapped coordinates

void ChainTrans(base_generator_type& rnd, const vector<dvec> &initChain, const dvec &boxLs, vector<dvec> &chain,double &transMag)
{
    //moves the chain somewhere so that the first point is still in the box
    chain = initChain;
    dvec move(DIM);
    dvec init(DIM);
    init=initChain[0];
    for (int dd = 0; dd < DIM; ++dd)
    {
        double rmin = -init[dd]+1e-5;
        //rmin = max(rmin, init[dd]-transMag);
        rmin = max(rmin, -transMag);
        double rmax = boxLs[dd]-init[dd]-1e-4;
        //rmax = min(rmax,init[dd]+transMag);
        rmax = min(rmax,transMag);
        move[dd]=random01(rnd)*(rmax-rmin)+rmin;
    };
    for (int mm = 0; mm < chain.size(); ++mm)
        for (int dd = 0; dd < DIM; ++dd)
            chain[mm][dd]+=move[dd];
};

void ChainSwap(vector<dvec> &c1,vector<dvec> &c2)
{
    //swaps the cm posisions of chain 1 and 2
    assert(c1.size()==c2.size());
    dvec cm1(DIM,0.0);
    dvec cm2(DIM,0.0);
    for (int nn=0; nn < c1.size(); ++nn)
        for (int dd= 0; dd< DIM; ++dd)
        {
            cm1[dd]+=c1[nn][dd];
            cm2[dd]+=c2[nn][dd];
        };
    for (int dd = 0; dd < DIM; ++dd)
    {
        cm1[dd] =cm1[dd] / (float)(c1.size());
        cm2[dd] =cm2[dd] / (float)(c1.size());
    };
    for (int nn=0; nn < c1.size(); ++nn)
        for (int dd = 0; dd <DIM; ++dd)
        {
            c1[nn][dd] = c1[nn][dd]-cm1[dd]+cm2[dd];
            c2[nn][dd] = c2[nn][dd]-cm2[dd]+cm1[dd];
        };
};

void ChainInvert(vector<dvec> &chain)
{
    //inverts the chain about its center of mass
    dvec cm(DIM,0.0);
    for (int nn=0; nn < chain.size(); ++nn)
        for (int dd= 0; dd< DIM; ++dd)
            cm[dd]+=chain[nn][dd];
    for (int dd = 0; dd < DIM; ++dd)
        cm[dd] =cm[dd] / (float)(chain.size());
    for (int nn = 0; nn < chain.size(); ++nn)
        for (int dd = 0; dd < DIM; ++dd)
            chain[nn][dd] = 2.0*cm[dd]-chain[nn][dd];
};

void ChainReflect(vector<dvec> &chain,dvec &vec)
{
    //reflect about a random mirror plane through the center of mass
    dvec cm(DIM,0.0);
    for (int nn=0; nn < chain.size(); ++nn)
        for (int dd= 0; dd< DIM; ++dd)
            cm[dd]+=chain[nn][dd];
    for (int dd = 0; dd < DIM; ++dd)
        cm[dd] =cm[dd] / (float)(chain.size());
    for (int nn = 0; nn < chain.size(); ++nn)
        for (int dd = 0; dd < DIM; ++dd)
            chain[nn][dd] = chain[nn][dd]-cm[dd];

    //set the reflection matrix
    assert(vec.size()==3);
    double norm = vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2];
    vector<dvec> reflection(DIM, vector<double>(DIM,0.0));
    reflection[0][0] = 1.0-2.0*vec[0]*vec[0]/norm;
    reflection[0][1] = -2.0*vec[0]*vec[1]/norm;
    reflection[0][2] = -2.0*vec[0]*vec[2]/norm;
    reflection[1][0] = -2.0*vec[0]*vec[1]/norm;
    reflection[1][1] = 1.0-2.0*vec[1]*vec[1]/norm;
    reflection[1][2] = -2.0*vec[1]*vec[2]/norm;
    reflection[2][0] = -2.0*vec[0]*vec[2]/norm;
    reflection[2][1] = -2.0*vec[1]*vec[2]/norm;
    reflection[2][2] = 1.0-2.0*vec[2]*vec[2]/norm;

    //each bead is the reflection about the cm, then with the cm restored
    for (int nn = 0; nn < chain.size(); ++nn)
    {
        dvec ans(DIM);
        matvecprod(reflection,chain[nn],ans);
        for (int dd = 0; dd < DIM; ++dd)
            ans[dd] +=cm[dd];
        chain[nn]=ans;
    };
};

void ChainRotate(vector<dvec> &chain,dvec &vec,double mag)
{
    //reflect about a random mirror plane through the center of mass
    dvec cm(DIM,0.0);
    for (int nn=0; nn < chain.size(); ++nn)
        for (int dd= 0; dd< DIM; ++dd)
            cm[dd]+=chain[nn][dd];
    for (int dd = 0; dd < DIM; ++dd)
        cm[dd] =cm[dd] / (float)(chain.size());
    for (int nn = 0; nn < chain.size(); ++nn)
        for (int dd = 0; dd < DIM; ++dd)
            chain[nn][dd] = chain[nn][dd]-cm[dd];

    //set the reflection matrix
    assert(vec.size()==3);
    double a2= vec[0]*vec[0];
    double b2 = vec[1]*vec[1];
    double c2 = vec[2]*vec[2];
    double ab = vec[0]*vec[1];
    double ac = vec[0]*vec[2];
    double bc = vec[1]*vec[2];
    double norm2 = a2+b2+c2;
    double norm = sqrt(norm2);
    double ni = 1.0/norm2;
    double c = cos(mag);
    double s = sin(mag);
    vector<dvec> rotate(DIM, vector<double>(DIM,0.0));

    rotate[0][0] = (a2+c*(b2+c2))*ni;
    rotate[0][1] = (ab-ab*c-vec[2]*norm*s)*ni;
    rotate[0][2] = (ac-ac*c+vec[1]*norm*s)*ni;
    rotate[1][0] = (ab-ab*c+vec[2]*norm*s)*ni;
    rotate[1][1] = (b2+c*(a2+c2))*ni;
    rotate[1][2] = (bc-bc*c-vec[0]*norm*s)*ni;
    rotate[2][0] = (ac-ac*c-vec[1]*norm*s)*ni;
    rotate[2][1] = (bc-bc*c+vec[0]*norm*s)*ni;
    rotate[2][2] = (c2+c*(a2+b2))*ni;

    //each bead is the reflection about the cm, then with the cm restored
    for (int nn = 0; nn < chain.size(); ++nn)
    {
        dvec ans(DIM);
        matvecprod(rotate,chain[nn],ans);
        for (int dd = 0; dd < DIM; ++dd)
            ans[dd] +=cm[dd];
        chain[nn]=ans;
    };
};
