//This header file contains utility functions related to a simple box implementation, and ways to keep chains/chain ends inside the primary unit cell


double mindistsquared(const dvec &posA,const dvec &posB, const dvec &boxl)
{
    dvec diff(posA.size());
    double prod = 0.0;
    for (int dd = 0; dd < posA.size(); ++dd)
    {
        diff[dd]=posA[dd]-posB[dd];
        if (fabs(diff[dd]) > 0.5*boxl[dd])
            diff[dd] -= sgn(diff[dd])*boxl[dd];
        prod += diff[dd]*diff[dd];
    };
    return prod;
}

double multipleReflections(double a, double x)
{
    //given a coordinate value a, reflect it multiple times across the wall at x and 0
    //until it is in the box
    if (a > 0 && a < x)
        return a;
    double ans = a;
    while (ans < 0 || ans >x)
    {
        if (ans < 0) ans = -ans;
        if (ans > x) ans = 2.0*x-ans;
    };
    return ans;
};

//functions relating chains to boxes



void keepInBoxNonperiodic(const dvec &point, const dvec &boxLs, const vector<int> &pbcs, dvec &ans)
{//takes a point and returns it to the box. If a non-periodic dimension is specified, reflect
 //point across the wall back into the box
    ans = point;

    for (int dd = 0; dd < DIM; ++dd)
    {
        if (point[dd] < 0)
        {
            if (pbcs[dd]!= 0)
                ans[dd]= multipleReflections(point[dd],boxLs[dd]);
            else
                while (ans[dd] < 0)
                    ans[dd] += boxLs[dd];
        };
        if (point[dd] > boxLs[dd])
        {
            if(pbcs[dd]!=0)
                ans[dd]= multipleReflections(point[dd],boxLs[dd]);
            else
                while (ans[dd] >= boxLs[dd])
                    ans[dd] -= boxLs[dd];
        };
    };
};


void putChainInBox(vector<dvec> &chain, const dvec &boxls, const vector<int> &pbcs)
{
//takes a chain and returns the coordinates of that chain mapped back into the box correctly
    vector<dvec> tchain;
    tchain = chain;
    dvec first(DIM);
    keepInBoxNonperiodic(chain[0],boxls,pbcs,first);
    chain[0]=first;
    for (int nn = 0; nn < chain.size()-1; ++nn)
    {
        dvec bond(DIM);
        dvec point(DIM);
        dvec ans(DIM);
        for (int dd = 0; dd < DIM; ++dd)
        {
            bond[dd] = tchain[nn+1][dd]-tchain[nn][dd];
            point[dd] = chain[nn][dd]+bond[dd];
        };
        keepInBoxNonperiodic(point,boxls,pbcs,ans);
        chain[nn+1]=ans;
    };
};

void ChainStartsInBox(vector<dvec> &chain, const dvec &boxls, const vector<int> &pbcs)
{
//takes a chain and makes sure it starts in the simulation box
//Also make sure the chain is properly reflected off of simulation box walls if needed,
//but do not periodically wrap the chain
    vector<dvec> tchain;
    tchain = chain;
    dvec first(DIM);
    keepInBoxNonperiodic(chain[0],boxls,pbcs,first);
    chain[0]=first;
    for (int nn = 0; nn < chain.size()-1; ++nn)
    {
        dvec bond(DIM);
        dvec point(DIM);
        for (int dd = 0; dd < DIM; ++dd)
        {
            bond[dd] = tchain[nn+1][dd]-tchain[nn][dd];
            point[dd] = chain[nn][dd]+bond[dd];
            if (point[dd] < 0 && pbcs[dd] !=0)
                point[dd] = multipleReflections(point[dd],boxls[dd]);
            if (point[dd] > boxls[dd] && pbcs[dd] !=0)
                point[dd] = multipleReflections(point[dd],boxls[dd]);
        };
        chain[nn+1]=point;
    };
};




/*
double mindistsquaredNoBox(const dvec &posA,const dvec &posB)
{
    dvec diff(posA.size());
    double prod = 0.0;
    for (int dd = 0; dd < posA.size(); ++dd)
    {
        diff[dd]=posA[dd]-posB[dd];
        prod += diff[dd]*diff[dd];
    };
    return prod;
}





std::vector<int> cellTuple(int i1, int i2, int i3)
{
    std::vector<int> ans(3,0);
    ans[0]=i1;
    ans[1]=i2;
    ans[2]=i3;
    return ans;
};

void nearbyCells(std::vector<int> &cell, vector<int> &max, std::vector< std::vector<int> > &ans)
{
    int i[3] = {cell[0]-1,cell[0],cell[0]+1};
    int j[3] = {cell[1]-1,cell[1],cell[1]+1};
    int k[3] = {cell[2]-1,cell[2],cell[2]+1};
    if (i[0] < 0 ) i[0] = max[0];
    if (i[2] > max[0]) i[2] = 0;
    if (j[0] < 0) j[0] = max[1];
    if (j[2] > max[1]) j[2] = 0;
    if (k[0] < 0) k[0] = max[2];
    if (k[2] > max[2]) k[2] = 0;

    int n =0;
    for (int ii = 0; ii < 3; ++ii)
    {
        for (int jj = 0; jj < 3; ++jj)
        {
            for (int kk = 0; kk < 3; ++kk)
            {
                ans[n] = cellTuple(i[ii],j[jj],k[kk]);
                n = n+1;
            };
        };
    };

};

void cellIndex(const dvec &pos, const dvec &boxsize, std::vector<int> &ans)
{
    for (int ii = 0; ii < 3; ++ii)
    {
        ans[ii] = (int)floor(pos[ii]/boxsize[ii]);
    };

};

void getCellIndices(const vector<dvec> &atoms, dvec &boxsize, vector< vector<int> > &cis, vector< vector<int> > &incell, vector<int> &max)
{
//    int numcells=1;
//    for (int dd = 0; dd < DIM; ++dd)
//        numcells *= (max[dd]+1);
    for (int aa = 0; aa < atoms.size(); ++aa)
    {
        std::vector<int> ct(DIM);
        ct[0] = (int)floor(atoms[aa][0]/boxsize[0]);
        ct[1] = (int)floor(atoms[aa][1]/boxsize[1]);
        ct[2] = (int)floor(atoms[aa][2]/boxsize[2]);
       // cellIndex(atoms[aa],boxsize,ct);
        cis[aa]=ct;
        int cind = ct[0]+(max[0]+1)*ct[1]+ct[2]*(max[0]+1)*(max[1]+1);
        incell[cind].push_back(aa);
    };
};
*/

