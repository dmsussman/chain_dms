DIR = ./

name=chainUtility
obj=$(name).o

prjDIR = .
prjOBJGQS = \
$(obj)

srcDIR = $(DIR)

srcOBJGQS = \

#Compiler
CPP=g++
CC=g++
FF=ifort

#Compiler flags
FLAGS = -w -Ofast #-Ofast #-opt-report-file opt_report.txt
FFLAGS = $(FLAGS)
CFLAGS = $(FLAGS)
LinkFLAGS = $(FLAGS)



INCLUDE = \
-I$(srcDIR) \
-I/usr/local/Cellar/boost/1.59.0/include/boost

LIBRARY = \
-L/usr/local/Cellar/boost/1.59.0/lib


FRULE = $(FF)  $(FFLAGS) $(INCLUDE) -c -o $@ $<
CRULE = $(CPP) $(CFLAGS) $(INCLUDE) -c -o $@ $<
ORULE = $(CPP) $(CFLAGS) -o $@ $(OBJGQS) $(LIBRARY) $(LINK)


StandardDependencies = \
	$(srcDIR)/*.h

OBJGQS = \
$(patsubst %,$(srcDIR)/%,$(srcOBJGQS)) \
$(patsubst %,$(prjDIR)/%,$(prjOBJGQS))


.f.o: 
	$(FRULE)

.cpp.o: 
	$(CRULE)

$(name).out: $(OBJGQS)
	$(ORULE)

#if any header file is changed, the project file gets recompiled.
$(obj): $(StandardDependencies)

clean:
	\rm $(OBJGQS)

